# Aplicación TODOs "Web 1.0" #

Código de aplicación "TODOs" usando Ajax "nativo".

* PHP y MySQL en el servidor, pero PHP sólo genera respuestas en JSON.
* Interfaz del lado del cliente utilizando HTML, CSS/Bootstrap, y Javascript/Ajax

Instalada y disponible en http://afrodita.inf.ucv.cl/~ifigueroa/Javascript/TODOs/Ajax

### Instalación ###

* Utilizar archivo TablaTODO.mysql para crear la tabla en la base de datos MySQL que usted utilizará. **Ojo que se puede ocupar la misma base de datos para las versiones "1.0", "Ajax" y "Ajax-jQuery".**

* Debe copiar el archivo `db.php.template`, y ponerle nombre 'db.php', y reemplazar:

```
#!php

$servername = "YOURSERVER";
$username = "YOURUSER";
$password = "YOURPASSWORD";
$database = "YOURDATABASE";
```

por los datos y credenciales correctas a su instancia de base de datos MySQL.