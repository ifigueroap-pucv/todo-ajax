<?php

require('db.php');

if ($result = $mysqli->query("SELECT * FROM TODO;")) {
  // Esto funciona en PHP >= 5.3 con mysql native driver "mysqlnd", que afrodita no tiene
  //echo json_encode($result->fetch_all(MYSQLI_ASSOC));
  $data = array();
  while ($row = $result->fetch_assoc()) {
    $data[] = $row;
  }
  echo json_encode($data);

} else {
  echo "Error: " . mysqli_error($mysqli);
}


?>
