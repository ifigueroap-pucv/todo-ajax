<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TO Do List Ajax!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <script type="text/javascript">

    function cargarTareaEnFormulario (t) {
	    var taskForm = document.getElementById('taskForm');
	    taskForm.taskTitleInput.value = t.titulo;
	    taskForm.taskId.value = t.id;
	    taskForm.taskDateInput.value = t.fecha;
	    taskForm.taskDetails.value = t.descripcion;
    }

    function cargarListadoTareas () {

	    var todoList = document.getElementById('todoList');

	    // Borrar los elementos hijos, para cuando se refresque la lista
	    while (todoList.firstChild) {
		    todoList.removeChild(todoList.firstChild);
	    }

	    
	    //alert("Cargando tareas desde el servidor, pero sin recargar la página");
	    var xmlhttp;

	    if (window.XMLHttpRequest) {
		    // IE7+, Firefox, Chrome, Opera, Safari
		    xmlhttp = new XMLHttpRequest();
	    } else {
		    // IE6, IE5
		    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    xmlhttp.onreadystatechange = function() {
		    if (xmlhttp.readyState == XMLHttpRequest.DONE && xmlhttp.status == 200) {
			    // Recibir y parsear respuesta para construir objetos
			    var tareas = JSON.parse(xmlhttp.responseText);

			    // Agregar las tareas a la lista
			    for (var i = 0; i < tareas.length; i++) {

				    // Normalizar formato fechas
				    tareas[i].fecha = tareas[i].fecha.replace(/ .*/g, '');
				    
				    var newA = document.createElement("a");
				    newA.appendChild(document.createTextNode(tareas[i].titulo + " - " + tareas[i].fecha));
				    newA.title = "Tarea " + tareas[i].titulo;
				    newA.href = "#";

				    // newA.onclick = function () { alert('hola tarea ' + tareas[i].titulo); };
				    
				    newA.onclick = function (t) { return function () {
					    cargarTareaEnFormulario(t);
				    }} (tareas[i]);
				    
				    var newLi = document.createElement("li");
				    newLi.id = "li" + tareas[i].id;
				    newLi.title = tareas[i].descripcion;
				    
				    newLi.appendChild(newA);
				    todoList.appendChild(newLi);
			    }
		    } else if (xmlhttp.readyState == XMLHttpRequest.DONE) {
			    alert('El servidor retornó status: ' + xmlhttp.status);
		    }
	    }

	    xmlhttp.open("GET", "tareas.php", true);
	    xmlhttp.send();	    
    }
    
    window.onload = function () {
	    cargarListadoTareas();
    }

    function ajaxSubmitForm () {	    
	    var xmlhttp;
	    if (window.XMLHttpRequest) {
		    // IE7+, Firefox, Chrome, Opera, Safari
		    xmlhttp = new XMLHttpRequest();
	    } else {
		    // IE6, IE5
		    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    xmlhttp.onreadystatechange = function() {
		    if (xmlhttp.readyState == XMLHttpRequest.DONE && xmlhttp.status == 200) {
			    cargarListadoTareas();
		    } else if (xmlhttp.readyState == XMLHttpRequest.DONE) {
			    alert('El servidor retornó status: ' + xmlhttp.status);
		    }
	    }

	    var taskForm = document.getElementById('taskForm');
	    var params = "taskId=" + taskForm.taskId.value;
	    params += "&taskTitleInput=" + taskForm.taskTitleInput.value;
	    params += "&taskDateInput=" + taskForm.taskDateInput.value;
	    params += "&taskDetails=" + taskForm.taskDetails.value;

	    xmlhttp.open("POST", "guardarTarea.php", true);
	    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(params);
    }

    function ajaxBorrarTarea () {
	    var taskForm = document.getElementById('taskForm');
	    
	    var xmlhttp;
	    if (window.XMLHttpRequest) {
		    // IE7+, Firefox, Chrome, Opera, Safari
		    xmlhttp = new XMLHttpRequest();
	    } else {
		    // IE6, IE5
		    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    xmlhttp.onreadystatechange = function() {
		    if (xmlhttp.readyState == XMLHttpRequest.DONE && xmlhttp.status == 200) {
			    cargarListadoTareas();
			    taskForm.reset();
			    
		    } else if (xmlhttp.readyState == XMLHttpRequest.DONE) {
			    alert('El servidor retornó status: ' + xmlhttp.status);
		    }
	    }
	    
	    var params = "taskId=" + taskForm.taskId.value;
	    
	    xmlhttp.open("POST", "borrarTarea.php", true);
	    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(params);
    }

    
    </script>

  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
	<div class="col-md-4">
	  <h3 class="text-center">
	    Listado de Tareas
	  </h3>
	  <button id="newTaskBtn" type="button" class="btn btn-default" onclick="location.href='index.php'">
	    Nueva Tarea
	  </button>

	  <ul id="todoList">	    
	  </ul>

	</div>
	<div class="col-md-8">
	  <h3 class="text-center">
	    Datos de Tarea
	  </h3>
	  <form role="form" id="taskForm" action="#">
	    <div class="form-group">
	      <input type="hidden" id="taskId" name="taskId"/>
	      <label for="taskTitleinput">
		Título
	      </label>
	      <input type="text" class="form-control" name="taskTitleInput" "id="taskTitleInput" placeholder="Título de la tarea...">
	    </div>

	    <div class="form-group">
	      <label for="taskDateInput">
		Fecha
	      </label>
	      <input type="date" class="form-control" id="taskDateInput" name="taskDateInput"/> 
	    </div>

	    <div class="form-group">
	      <label for="taskDetails">
		Detalles
	      </label>
	      <textarea name="taskDetails" form="taskForm" class="form-control" placeholder="Descripción detallada de la tarea..."></textarea>
	    </div>

	    <button type="button" class="btn btn-primary"onclick="ajaxSubmitForm();">
	      Guardar
	    </button>
	    
	    <button type="reset" class="btn btn-danger">
	      Limpiar
	    </button>	    
	    <button id="deleteTaskBtn" type="button" class="btn btn-danger" onclick="ajaxBorrarTarea();">
	      Borrar
	    </button>
	  </form>
	</div>
      </div>
    </div>   
  </body>
</html>
